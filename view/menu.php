<div id="sidebar-menu">
                        <ul>

                        	

                            <li class="">
                                <a href="index.php" ><i class="ti-home"></i> <span> Dashboard </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-linux"></i> <span> Customer </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="customer-create.php">Create</a></li>
                                    <li><a href="suppliers.php">List of Customers</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-light-bulb"></i><span> Items </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="components-grid.html">New Items</a></li>
                                    <li><a href="components-widgets.html">List Of Items</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-spray"></i> <span> Supplier </span> </a>
                                <ul class="list-unstyled">
                                	<li><a href="suppliers.php">Suppliers</a></li>
                                    <li><a href="create-suppliers.php">New Suppliers</a></li>
                                   
                                </ul>
                            </li>
							
							<li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-spray"></i> <span> Company </span> </a>
                                <ul class="list-unstyled">
                                	<li><a href="suppliers.php">Companies </a></li>
                                    <li><a href="create-suppliers.php">New Company</a></li>
                                   
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-bookmark-alt"></i><span> Reports </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="form-elements.html">Suppliers</a></li>
                                    <li><a href="form-advanced.html">Customers</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-server"></i><span>Products </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tables-basic.html">Receive</a></li>
                                    <li><a href="tables-datatable.html">Sales</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-cut"></i><span> Expenses </span></a>
                                <ul class="list-unstyled">
                                	<li><a href="chart-flot.html">Add</a></li>
                                    <li><a href="chart-morris.html">List Of Expenses</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-location-pin"></i><span> Employees </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="map-google.html"> New Employee</a></li>
                                    <li><a href="map-vector.html"> List of Employees</a></li>
                                </ul>
                            </li>

                          




                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-settings"></i><span> configuration  </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="ecommerce-dashboard.html"> Company Information</a></li>
                                    
                                </ul>
                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>